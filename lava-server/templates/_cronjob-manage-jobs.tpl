{{/*
LAVA Jobs management task run at given schedule.
*/}}
{{- define "lava-server.cronjob-manage-jobs" -}}
apiVersion: batch/v1
kind: CronJob
metadata:
  name: {{ include "lava-server.fullname" . }}-{{ .Args.name }}
spec:
  schedule: {{ .Args.schedule | quote }}
  concurrencyPolicy: Forbid
  jobTemplate:
    spec:
      template:
        spec:
          containers:
          - name: {{ .Chart.Name }}
            image: "{{ .Values.image.repository }}:{{ .Values.image.tag }}"
            imagePullPolicy: {{ .Values.image.pullPolicy }}
            command: ["/bin/sh"]
            args:
              - "-c"
              - "lava-server manage jobs {{ .Args.command }} --older-than {{ .Args.olderThan }}"
            env:
              - name: SECRET_KEY
                valueFrom:
                  secretKeyRef:
                    name: lava-server-django
                    key: secretKey
              {{- toYaml .Values.lavaEnvironment | nindent 14 }}
            volumeMounts:
              - mountPath: /etc/lava-server
                name: data
                subPath: configuration
              - mountPath: /var/lib/lava-server
                name: data
                subPath: data
          restartPolicy: OnFailure
          volumes:
          - name: data
            persistentVolumeClaim:
              claimName: lava-server
          affinity:
            podAffinity:
              requiredDuringSchedulingIgnoredDuringExecution:
              - labelSelector:
                  matchExpressions:
                    - key: app.kubernetes.io/name
                      operator: In
                      values:
                        - {{ include "lava-server.fullname" . }}
                topologyKey: "kubernetes.io/hostname"
{{- end }}
